Pod::Spec.new do |s|

  s.name         = "MicroCalculator"
  s.version      = "0.0.1"
  s.summary      = "A demo for integrating a Swift framework in an Objective C app."

  s.description  = <<-DESC
                   This is just a demo for integrating a Swift framework in an Objective C app.
                   DESC

  s.homepage     = "https://bitbucket.org/bennokress/microcalculator"
  s.license      = { :type => 'NONE', :text => <<-LICENSE
    License is hereby granted to everyone.
    LICENSE
  }
  s.author       = "BK | Mobile Apps"
  s.social_media_url   = "http://twitter.com/bennokress"

  s.platform     = :ios, '8.0'
  
  s.source       = { :git => "https://bitbucket.org/bennokress/microcalculator.git", :tag => "#{s.version}" }
  s.source_files = "MicroCalculator/**/*.{swift,h,m}"

  s.requires_arc = true
  s.dependency 'SwifterSwift'

end
